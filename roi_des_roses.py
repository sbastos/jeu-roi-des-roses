import random
from typing import Mapping

##########################################
####### 1 - Initialisation du jeu ########
##########################################
def init_jeu(nb_lignes, nb_colonnes):
    # Construction du plateau sous forme de matrice avec les dimensions appropriées
    plateau = []
    plateau1 = []
    for i in range(nb_lignes):
        plateau.append(plateau1*nb_lignes)
        for _ in range(nb_colonnes):
            plateau[i].append("")
    # Positionner le roi au milieu du plateau
    l_roi = nb_lignes // 2 
    c_roi = nb_colonnes // 2
    plateau[l_roi][c_roi] = 'X'
    # Pioche mélangée
    pioche = ["N1", "N2", "N3", "NE1", "NE2", "NE3", "E1", "E2", "E3", "SE1", "SE2", "SE3", "S1", "S2", "S3", "SO1", "SO2", "SO3", "O1", "O2", "O3", "NO1", "NO2", "NO3"]
    random.shuffle(pioche)
    # Main pour le joueur Rouge
    main_r = []
    for i in range(5):
        x = random.randint(1, len(pioche) - 1)
        main_r.append(pioche[x])
        del pioche[x]
    # Main pour le joueur Blanc
    main_b = []
    for i in range(5):
        x = random.randint(1, len(pioche) - 1)
        main_b.append(pioche[x])
        del pioche[x]
    # Defausse
    defausse = []
    return(plateau, l_roi, c_roi, main_r, main_b, pioche, defausse)

##########################################
##### 2 - Affichage de l'état du jeu #####
##########################################
def afficher_main(couleur, main):
    main=" ".join(main)
    if couleur == "Rouge" :
        print ("Rouge : "+ main)
    else :
        print ("Blanc :" , main)

def afficher_jeu(plateau, l_roi, c_roi, main_r, main_b):
    # Affichage des mains des 2 joueurs :
    afficher_main("Rouge", main_r)
    afficher_main("Blanc", main_b)
    # Affichage du plateau :
    nb_colonnes = len(plateau[1])
    for k in range(len(plateau)):
        print((("-")*nb_colonnes*5) + ("-"), end = '\n')
        for l in range(len(plateau[k])):
            # Affichage si aucun pion n'est présent sur la case :
            if plateau[k][l] == '':
                print("|  ",plateau[k][l],"",end = '')
            # Affichage avec le roi  + un pion :
            elif len(plateau[k][l]) == 2:
                print("|",plateau[k][l],"", end = '')
            # Affichage avec le roi seul OU un pion seul :
            elif plateau[k][l] == 'R' or plateau[k][l] == 'B' or plateau[k][l] == 'X':
                if plateau[k][l] == 'X':
                    print("|",plateau[k][l]," ", end = '')
                else:
                    print("| ",plateau[k][l],"", end = '')
        print("|\n", end = '')
    print((("-")*nb_colonnes*5) + ("-"), end = '\n')

##########################################
##### 3 - Mouvement du roi possible ######
##########################################
def mouvement_possible(plateau, l_roi, c_roi, carte):
    # On initie deux variables nb_lignes, nb_colonnes permettant
    # de poser les limites du plateau
    nb_lignes = len(plateau)
    nb_colonnes = len(plateau[0])
    # Pour chaque carte possible du jeu et pour chaque position on
    # return False si le mouvement n'est pas possible
    # et True si il l'est.
    # 1/ Cartes "N"
    if carte == "N1":
        # Si la ligne du roi = 0 dans ce cas on arrive en dehors du plateau
        # --> pas possible
        if l_roi == 0:
            return False
        l_roi = l_roi - 1
        # Si un pion est présent sur les nouvelles coordonnées après le déplacement avec na carte
        # --> pas possible
        if plateau[l_roi][c_roi] != "":
            return False
        return True
        # Sinon, on joue, le déplacement est possibles
    elif carte == "N2":
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "N3":
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 2/ Cartes "NE"
    elif carte == "NE1":
        if c_roi == nb_colonnes - 1:
            return False
        elif l_roi == nb_colonnes - 1:
            return False
        l_roi = l_roi - 1
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NE2":
        if c_roi > nb_colonnes - 3:
            return False
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        C_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NE3":
        if c_roi > nb_colonnes - 4:
            return False
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 3/ Cartes "E"
    elif carte == "E1":
        if c_roi == nb_colonnes - 1:
            return False
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "E2":
        if c_roi > nb_colonnes - 3:
            return False
        c_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "E3":
        if c_roi > nb_colonnes - 4:
            return False
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 4/ Cartes "SE":
    elif carte == "SE1":
        if c_roi == nb_colonnes - 1:
            return False
        if l_roi == nb_lignes - 1:
            return False
        l_roi = l_roi + 1
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SE2":
        if c_roi > nb_colonnes - 3:
            return False
        if l_roi > nb_lignes - 3:
            return False
        l_roi = l_roi + 2
        c_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SE3":
        if c_roi > nb_colonnes - 4:
            return False
        if l_roi > nb_lignes - 4:
            return False
        l_roi = l_roi + 3
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 5/ Cartes "S"
    elif carte == "S1":
        if l_roi == nb_lignes - 1:
            return False
        l_roi = l_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "S2":
        if l_roi > nb_lignes - 3:
            return False
        l_roi = l_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "S3":
        if l_roi > nb_lignes - 4:
            return False
        l_roi = l_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 6/ Cartes "SO"
    elif carte == "SO1":
        if l_roi == nb_lignes - 1:
            return False
        if c_roi == 0:
            return False
        l_roi = l_roi + 1
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SO2":
        if l_roi > nb_lignes - 3:
            return False
        if c_roi < 2:
            return False
        l_roi = l_roi + 2
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SO3":
        if l_roi > nb_lignes - 4:
            return False
        if c_roi < 3:
            return False
        l_roi = l_roi + 3
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 7/ Cartes "O"
    elif carte == "O1":
        if c_roi == 0:
            return False
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "O2":
        if c_roi < 2:
            return False
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "O3":
        if c_roi < 3:
            return False
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 8/ Cartes "NO"
    elif carte == "NO1":
        if c_roi == 0:
            return False
        if l_roi == 0:
            return False
        l_roi = l_roi - 1
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NO2":
        if c_roi < 2:
            return False
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NO3":
        if c_roi < 3:
            return False
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True

##########################################
########### 4 - Main jouable #############
##########################################
def main_jouable(plateau, l_roi, c_roi, main):
    liste_main_jouable = []
    for i in range(len(main)):
        x = mouvement_possible(plateau, l_roi, c_roi, main[i])
        if x == True:
            liste_main_jouable.append(main[i])
    return liste_main_jouable
# main_jouable renvoie la liste des cartes de la main pour lesquelles le 
# mouvement est possible, si aucun n'est possible alors une liste vide est return

###################################################
######## 5 - Demande une action à un joueur #######
###################################################
def demande_action(couleur, plateau, l_roi, c_roi, main):
    choix_carte = input ("Vous êtes le joueur "+couleur+", que voulez vous faire ? ")
    while True :
        # Si le choix est "pioche" on vérifie que le joueur n'a pas 5 cartes déjà dans sa main
        # Si c'est le cas --> impossible de piocher
        if choix_carte == "pioche":
            if len(main) == 5:
                choix_carte = input("Action impossible, que souhaitez-vous faire ? ")
            else:
                break
        elif choix_carte == "passe":
            # Si le choix est "passe" on vérifie qu'il ne peut pas piocher + qu'il ne peut pas jouer une seule
            # des cartes de sa main
            for _ in main:
                if len(main_jouable(plateau, l_roi, c_roi, main)) != 0 and len(main) < 5:
                    choix_carte = input("Action impossible, que souhaitez-vous faire ? ") 
            return choix_carte
        # Si le choix n'est pas "pioche" ou "passe", on vérifie que le choix est une carte jouable dans sa main
        else :
            if choix_carte not in main and choix_carte != main:
                choix_carte = input("Action impossible, que souhaitez-vous faire ? ") 
            else :
                if mouvement_possible(plateau, l_roi, c_roi, choix_carte) == False:
                    choix_carte = input("Action impossible, que souhaitez-vous faire ? ")
                else:
                    break
    return choix_carte

##########################################
############ 6 - Bouge le roi ############
##########################################
# Fonction préliminaire pour bouge_le_roi permettant de déplacer le roi
def mouvement_pionroi(couleur, plateau, l_roi, c_roi, carte):
    if mouvement_possible(plateau, l_roi, c_roi, carte) == True:
        # Enlever le roi de la position d'origine (l_roi, c_roi)
        roi = "X"
        plateau[l_roi][c_roi] = "".join(x for x in plateau[l_roi][c_roi] if x not in roi)
        # En fonction des cartes, ajouter le pion + le roi (si le mouvement est possible)
        if len(carte) == 2:
            direction = carte[0]
            case = carte[1]
            case = int(case)
            if direction == "N":
                l_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "S":
                l_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "E":
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "O":
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
        elif len(carte) == 3:
            direction = carte[0:2]
            case = carte[2]
            case = int(case)
            if direction == "NE":
                l_roi -= case
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "NO":
                l_roi -= case
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "SE":
                l_roi += case
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "SO":
                l_roi += case
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
    return(plateau, l_roi, c_roi)

def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, couleur):
    # Pour déplacer le roi et mettre le pion de la bonne couleur
    x = mouvement_pionroi(couleur, plateau , l_roi, c_roi, carte)
    # On récupère les variables de mouvement_pionroi pour les mettre à jour pour la suite du jeu
    plateau = x[0]
    l_roi = x[1]
    c_roi = x[2]
    # Retirer la carte jouée de la main du joueur
    if couleur == "Rouge":
        index_carte = main_r.index(carte)
        del main_r[index_carte]
    if couleur == "Blanc":
        index_carte = main_b.index(carte)
        del main_b[index_carte]
    # Ajouter la carte jouée dans la défausse
    defausse.append(carte)
    return(plateau, l_roi, c_roi, main_r, main_b, defausse)

###############################################
######## 7 - Définition des territoires #######
###############################################
def territoire(plateau, ligne, colonne, couleur):
    territoire = []
    # Pour le joueur Blanc
    if couleur == "Blanc": 
        pos_Blanc_plateau = []
        # S'il n'y a pas de pion Blanc sur la case (ligne, colonne), renvoie une liste vide :
        if plateau[ligne][colonne] != 'B' and plateau[ligne][colonne] != 'XB':
            return territoire
        # Lister toutes les positions des pions blancs ('B' ou 'XB) sur le plateau :
        else:
            for i in range(len(plateau)):
                for j in range(len(plateau[i])):
                    if plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                        pos_Blanc_plateau.append((i,j))
                # Algo pour trouver le territoire blanc à partir de la case (ligne, colonne):
                territoire_Blanc = [(ligne, colonne)]
                q = 0
                while q < len(territoire_Blanc):
                    ligne = territoire_Blanc[q][0]
                    colonne = territoire_Blanc[q][1]
                    # Pour la première itération (ligne, colonne du départ) on regarde les côtés adjacents (pas de diagonales)
                    # si un pion de la couleur blanc est détecté alors on prend ce pion et on refait la même chose
                    l = [(ligne-1, colonne), (ligne+1, colonne), (ligne, colonne-1), (ligne, colonne+1)]
                    for elem_l in range(len(l)):
                        for elem_pion in range(len(pos_Blanc_plateau)):
                            if l[elem_l] == pos_Blanc_plateau[elem_pion]:
                                if l[elem_l] not in territoire_Blanc:
                                    territoire_Blanc.append(l[elem_l]) 
                    q += 1               
            return territoire_Blanc
    elif couleur == "Rouge": 
        pos_Rouge_plateau = []
        # S'il n'y a pas de pion Blanc sur la case (ligne, colonne), renvoie une liste vide:
        if plateau[ligne][colonne] != 'R' and plateau[ligne][colonne] != 'XR':
            return territoire
        # Lister toutes les positions des pions rouges ('R' ou 'XR') sur le plateau :
        else:
            for i in range(len(plateau)):
                for j in range(len(plateau[i])):
                    if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                        pos_Rouge_plateau.append((i,j))
                    # Algo pour trouver le territoire Rouge à partir de (ligne, colonne):
                    territoire_Rouge = [(ligne, colonne)]
                    q = 0
                    while q < len(territoire_Rouge):
                        ligne = territoire_Rouge[q][0]
                        colonne = territoire_Rouge[q][1]
                        l = [(ligne-1, colonne), (ligne+1, colonne), (ligne, colonne-1), (ligne, colonne+1)]
                        for elem_l in range(len(l)):
                            for elem_pion in range(len(pos_Rouge_plateau)):
                                if l[elem_l] == pos_Rouge_plateau[elem_pion]:
                                    if l[elem_l] not in territoire_Rouge:
                                        territoire_Rouge.append(l[elem_l]) 
                        q += 1                       
            return territoire_Rouge

##########################################
############## 8 - Scores ################
##########################################
def score(plateau, couleur):
    # Pour le joueur Rouge
    if couleur == "Rouge":
        # Lister toutes les positions du jeu avec des pions rouges
        pos_Rouge_plateau = []
        for i in range(len(plateau)):
            for j in range(len(plateau[i])):
                if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                    pos_Rouge_plateau.append((i,j))
        # Algo du score pour le joueur Rouge
        score_Rouge = 0
        m = len(pos_Rouge_plateau) - 1
        while m > -1:
            terr = territoire(plateau, pos_Rouge_plateau[m][0], pos_Rouge_plateau[m][1], "Rouge")
            terr2 = []
            for elem in terr:
                if elem in pos_Rouge_plateau:
                    terr2.append(elem)
            score_Rouge += len(terr2)**2
            for elem in terr2:
                pos_Rouge_plateau.remove(elem)  
            m -= len(terr2)
        return score_Rouge
    elif couleur == "Blanc":
        # Lister toutes les positions du jeu avec des pions blancs
        pos_Blanc_plateau = []
        for i in range(len(plateau)):
            for j in range(len(plateau)):
                if plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                    pos_Blanc_plateau.append((i,j))
        # Algo du score pour le joueur Blanc
        score_Blanc = 0
        m = len(pos_Blanc_plateau) - 1
        while m > -1:
            terr = territoire(plateau, pos_Blanc_plateau[m][0], pos_Blanc_plateau[m][1], "Blanc")
            terr2 = []
            for elem in terr:
                if elem in pos_Blanc_plateau:
                    terr2.append(elem)
            score_Blanc += len(terr2)**2
            for elem in terr2:
                pos_Blanc_plateau.remove(elem)
            m -= len(terr2)
        return score_Blanc

#############################################
######## 9 - Boucle de jeu principale #######
#############################################
def main ():
    # Initialisation de toutes les variables 
    init = init_jeu(9,9)
    plateau = init[0]
    l_roi = init[1]
    c_roi = init[2]
    main_r = init[3]
    main_b = init[4]
    pioche = init[5]
    defausse = init[6]
    # Initialisation des variables "passe"
    passe = []
    # Début de la boucle de jeu principale
    x = True
    # Le rouge commence toujours
    couleur = "Rouge"
    main = main_r
    nombre_tour = 0
    while x == True :
        compteur_pion_rouge = 0
        compteur_pion_blanc = 0
        for i in range(len(plateau)):
            for j in range(len(plateau[i])):
                if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                    compteur_pion_rouge += 1
                elif plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                    compteur_pion_blanc += 1
        if compteur_pion_blanc == 26 and compteur_pion_rouge == 26:
            x = False
        elif len(passe) == 2:
            x = False
        elif nombre_tour > 0:
            if couleur == "Rouge":
                couleur = "Blanc"
                main = main_b
            elif couleur == "Blanc":
                couleur = "Rouge"
                main = main_r
        elif compteur_pion_rouge == 26 and compteur_pion_blanc < 26:
            couleur = "Blanc"
            main = main_b
        elif compteur_pion_blanc == 26 and compteur_pion_rouge < 26:
            couleur = "Rouge"
            main = main_r
        afficher_jeu(plateau, l_roi, c_roi, main_r, main_b)
        action = demande_action(couleur, plateau, l_roi, c_roi, main)
        if action == "passe":
            passe.append(couleur)
            passe = list(set(passe))
            nombre_tour += 1
        elif action == "pioche":
            # Si la pioche est vide, alors on transfère le contenu de la défausse dans 
            # la pioche et on mélange
            if len(pioche) == 0:
                i = len(defausse) - 1 
                while i > -1:
                    pioche.append(defausse[i])
                    del defausse[i]
                    i -= 1
                random.shuffle(pioche)
            # Si la pioche n'est pas vide:
            pioche_carte = random.choice(pioche)
            main.append(pioche_carte)
            i = pioche.index(pioche_carte)
            del pioche[i]
            nombre_tour += 1
        else :
            mouvement = bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, action, couleur)
            plateau = mouvement[0]
            l_roi = mouvement[1]
            c_roi = mouvement[2]
            nombre_tour += 1
    score_Rouge = score(plateau, "Rouge")
    score_Blanc = score(plateau, "Blanc")
    print("Rouge", score_Rouge)
    print("Blanc", score_Blanc)
    if score_Rouge > score_Blanc:
        print("Rouge a gagné la partie")
    elif score_Rouge == score_Blanc:
        print("Égalité") 
    else:
        print("Blanc a gagné la partie")

if __name__ == "__main__":
    main() 