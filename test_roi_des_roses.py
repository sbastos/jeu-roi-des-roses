'''
Test pour le roi des roses
'''
import pytest
from roi_des_roses import *

class Test_init_jeu:
    # Test des dimensions du plateau selon les entrées de la fonction
    def test_plateau(self):
        # Plateau 9x9
        assert len(init_jeu(9,9)[0]) == 9
        assert len(init_jeu(9,9)[0][1]) == 9
        # Plateau 13x13
        assert len(init_jeu(13,13)[0]) == 13
        assert len(init_jeu(13,13)[0][1]) == 13
        # Plateau 15x15
        assert len(init_jeu(15,15)[0]) == 15
        assert len(init_jeu(15,15)[0][1]) == 15
    # Test des coordonnées du roi en début de partie en fonction des dimensions (toujours au milieu)
    def test_ligne_et_colonneroi(self):
        # Plateau 9x9
        assert init_jeu(9,9)[1] and init_jeu(9,9)[2] == 4
        # Plateau 13x13
        assert init_jeu(13,13)[1] and init_jeu(13,13)[2] == 6
        # Plateau 15x15
        assert init_jeu(15,15)[1] and init_jeu(15,15)[2] == 7
    # Test de la longueur de la main rouge en début de partie 
    def test_main_Rouge(self):
        assert len(init_jeu(9,9)[3]) == 5
    # Test de la longueur de la main blanc en début de partie
    def test_main_Blanc(self):
        assert len(init_jeu(9,9)[4]) == 5
    # Test de la longueur de la pioche en début de partie (pioche = 24 - main_r - main_b)
    def test_pioche(self):
        assert len(init_jeu(9,9)[5]) == 14
    # Test de la longueur de la défausse en début de partie (vide)
    def test_defausse(self):
        assert len(init_jeu(9,9)[6]) == 0

class Test_MouvementPossible:
    # Test mouvement sur un pion déjà posé
    def test_Mouvement1(self):
        plateau = [['XB', 'B', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', 'B']]
        assert mouvement_possible(plateau, 0, 0, "E1") == False
    # Test mouvement en dehors du plateau
    def test_Mouvement2(self):
        plateau = [['B', 'B', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', 'XB']]
        assert mouvement_possible(plateau, 8, 8, "SO3") == False
    # Test sur une case vide
    def test_Mouvement3(self):
        plateau = [['B', 'B', '', 'R', '', '', '', 'R', 'XR'], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', 'B']]
        assert mouvement_possible(plateau, 0, 8, "S1") == True

class Test_MainJouable:
    # Plateau en début de partie donc toutes les cartes dans la main du joueur sont jouables
    def test_DebutDePartie(self):
        plateau = [['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', 'X', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']]
        assert main_jouable(plateau, 4, 4, ["SO2", "N2", "S3", "S2", "NE2"]) == ["SO2", "N2", "S3", "S2", "NE2"]
    # Test avec aucun mouvement possible de la part du joueur
    def test_AucunMouvementPossible(self):
        plateau = [['B', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'XB'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert main_jouable(plateau, 6, 8, ["O2", "O1", "S02", "SO3", "N2"]) == []
    # Test qui affiche quelques mouvement possible pour le joueur
    def test_QuelquesMouvementPossible(self):
        plateau = [['XB', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert main_jouable(plateau, 0, 0, ["E2", "E1", "S02", "SO3", "N2"]) == ["E2", "E1"]
  
class Test_BougeLeRoi:
    def test_situation01_rouge(self):
        plateau = [['XB', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        l_roi = 0
        c_roi = 0
        main_r =  ["SO3", "E2", "N2"]
        main_b = ["S2", "SE2", "S1"]
        defausse = ["NO1", "NO2", "N1", "O1", "O3"]
        # Test pour le joueur Rouge de jouer la carte "E2" qui emmènerait le roi en (0,2)
        plateau_modifie_apres_le_coup =[['B', '', 'XR', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        x = bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, "E2", "Rouge")
        assert x == (plateau_modifie_apres_le_coup, 0, 2, ["SO3", "N2"], ["S2", "SE2", "S1"], ["NO1", "NO2", "N1", "O1", "O3", "E2"])
    def test_situation02_blanc(self):
        plateau = [['B', '', 'XR', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        l_roi = 0
        c_roi = 2
        main_r =  ["SO3", "N2"]
        main_b = ["S2", "SE2", "S1"]
        defausse = ["NO1", "NO2", "N1", "O1", "O3", "E2"]
        # Test pour le joueur Blanc de jouer la carte "SE2" qui emmènerait le roi en (2,4)
        plateau_modifie_apres_le_coup =[['B', '', 'R', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', 'XB', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'B'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        x = bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, "SE2", "Blanc")
        assert x == (plateau_modifie_apres_le_coup, 2, 4, ["SO3", "N2"], ["S2", "S1"], ["NO1", "NO2", "N1", "O1", "O3", "E2", "SE2"]) 


class Test_Territoire:
    def test_CaseVide(self):
        plateau = [['B', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'XB'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert territoire(plateau, 0, 1, "Rouge") == []
        assert territoire(plateau, 0, 1, "Blanc") == []
    # Territoire absent car pas de la bonne couleur
    def test_PasDePionDeLaBonneCouleur(self):
        plateau = [['B', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'XB'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert territoire(plateau, 0, 0, "Rouge") == []
        assert territoire(plateau, 0, 3, "Blanc") == []
    # Test avec des territoires avec une seule case
    def test_UnSeulPionDeLaBonneCouleur(self):
        plateau = [['B', '', '', 'R', '', '', '', 'R', ''], ['', 'R', 'R', '', 'R', '', 'B', 'B', ''], ['', '', 'R', '', '', 'B', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'XB'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert territoire(plateau, 0, 0, "Blanc") == [(0,0)]
        assert territoire(plateau, 0, 3, "Rouge") == [(0,3)]
    # Test avec des territoires > 1 case
    def test_GrosTerritoire(self):
        plateau = [['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], ['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], ['', 'R', 'R', 'R', '', 'R', '', '', 'R'], ['', '', 'B', 'R', 'B', 'B', 'R', 'R', ''], ['', '', '', '', 'B', 'B', 'B', '', 'B'], ['', '', '', 'R', 'R', 'B', 'R', 'B', 'B'], ['', '', '', 'B', 'B', 'R', 'R', 'R', 'XB'], ['', '', '', 'R', 'R', 'B', 'B', 'R', ''], ['', '', '', '', '', 'R', 'B', 'R', '']]
        assert len(territoire(plateau, 0, 0, "Blanc")) == 18
        assert len(territoire(plateau, 2, 2, "Rouge")) == 4

class Test_Scores:
    # Test sans pions (attendu : 0 point)
    def test_pas_de_pion_rouge(self):
        assert score([['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Rouge") == 0
    def test_pas_de_pion_blanc(self):
        assert score([['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Blanc") == 0
    # Test de l'addition des territoires du plateau (attendu : 9 pions isolés = 9 points)
    def test_9pions_rouges_isolés(self):
        assert score([['R', '', 'R', '', 'R', '', 'R', '', 'R'], ['', 'R', '', 'R', '', 'R', '', 'R', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Rouge") == 9
    def test_9pions_blancs_isolés(self):
        assert score([['B', '', 'B', '', 'B', '', 'B', '', 'B'], ['', 'B', '', 'B', '', 'B', '', 'B', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Blanc") == 9
    # Test avec un territoire énorme de 15 pions rouges (attendu : 15*15 = 225)
    def test_gros_territoire_rouge(self):    
        assert score([['R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R'], ['R', 'R', 'R', 'R', 'R', 'R', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Rouge") == 225
    def test_gros_territoire_blanc(self): 
        assert score([['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], ['B', 'B', 'B', 'B', 'B', 'B', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '', '', '']], "Blanc") == 225
    # Test sur une situation de jeu réelle n°1 : résultat attendu --> Rouge : 588 ; Blanc : 141
    def test_situation01(self):
        assert score([['', '', '', '', '', '', 'B', 'B', ''], ['', '', 'R', 'B', 'R', 'B', 'B', '', ''], ['B', 'B', 'R', 'R', 'R', 'B', 'R', 'R', 'R'], ['', 'B', 'B', 'B', 'R', 'R', 'B', 'B', 'B'], ['B', 'B', 'R', 'R', 'B', 'R', 'B', '', 'B'], ['', '', 'B', 'R', 'B', 'R', 'R', 'R', ''], ['B', 'B', 'R', 'R', 'R', 'R', 'XR', 'B', 'R'], ['B', 'R', 'R', 'R', 'R', 'B', '', 'R', 'B'], ['', '', '', 'R', 'B', 'B', 'B', 'B', 'R']], "Rouge") == 588
        assert score([['', '', '', '', '', '', 'B', 'B', ''], ['', '', 'R', 'B', 'R', 'B', 'B', '', ''], ['B', 'B', 'R', 'R', 'R', 'B', 'R', 'R', 'R'], ['', 'B', 'B', 'B', 'R', 'R', 'B', 'B', 'B'], ['B', 'B', 'R', 'R', 'B', 'R', 'B', '', 'B'], ['', '', 'B', 'R', 'B', 'R', 'R', 'R', ''], ['B', 'B', 'R', 'R', 'R', 'R', 'XR', 'B', 'R'], ['B', 'R', 'R', 'R', 'R', 'B', '', 'R', 'B'], ['', '', '', 'R', 'B', 'B', 'B', 'B', 'R']], "Blanc") == 141
    # Test sur une situation de jeu réelle n°2 (score plus serré): résultat attendu --> Rouge : 178 ; Blanc : 160
    def test_situation02(self):
        assert score([['', 'B', 'R', 'B', '', '', '', '', ''], ['', 'B', 'R', 'R', 'B', '', '', '', ''], ['B', 'B', 'R', 'R', 'R', 'R', '', '', ''], ['B', '', 'B', 'B', 'R', 'R', 'R', '', ''], ['B', 'R', 'R', 'B', '', 'B', 'B', 'B', ''], ['B', 'R', 'B', 'B', 'R', 'R', 'B', 'R', 'R'], ['B', 'B', '', 'R', 'B', 'R', 'R', 'B', ''], ['R', 'B', 'XR', 'R', 'B', 'R', 'R', '', ''], ['R', '', 'R', 'R', 'B', 'B', '', '', '']], "Rouge") == 178
        assert score([['', 'B', 'R', 'B', '', '', '', '', ''], ['', 'B', 'R', 'R', 'B', '', '', '', ''], ['B', 'B', 'R', 'R', 'R', 'R', '', '', ''], ['B', '', 'B', 'B', 'R', 'R', 'R', '', ''], ['B', 'R', 'R', 'B', '', 'B', 'B', 'B', ''], ['B', 'R', 'B', 'B', 'R', 'R', 'B', 'R', 'R'], ['B', 'B', '', 'R', 'B', 'R', 'R', 'B', ''], ['R', 'B', 'XR', 'R', 'B', 'R', 'R', '', ''], ['R', '', 'R', 'R', 'B', 'B', '', '', '']], "Blanc") == 160
