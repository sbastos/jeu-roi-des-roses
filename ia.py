import random
from typing import Mapping

##########################################
####### 1 - Initialisation du jeu ########
##########################################
def init_jeu(nb_lignes, nb_colonnes):
    # Construction du plateau sous forme de matrice avec les dimensions appropriées
    plateau = []
    plateau1 = []
    for i in range(nb_lignes):
        plateau.append(plateau1*nb_lignes)
        for _ in range(nb_colonnes):
            plateau[i].append("")
    # Positionner le roi au milieu du plateau
    l_roi = nb_lignes // 2 
    c_roi = nb_colonnes // 2
    plateau[l_roi][c_roi] = 'X'
    # Pioche mélangée
    pioche = ["N1", "N2", "N3", "NE1", "NE2", "NE3", "E1", "E2", "E3", "SE1", "SE2", "SE3", "S1", "S2", "S3", "SO1", "SO2", "SO3", "O1", "O2", "O3", "NO1", "NO2", "NO3"]
    random.shuffle(pioche)
    # Main pour le joueur Rouge
    main_r = []
    for i in range(5):
        x = random.randint(1, len(pioche) - 1)
        main_r.append(pioche[x])
        del pioche[x]
    # Main pour le joueur Blanc
    main_b = []
    for i in range(5):
        x = random.randint(1, len(pioche) - 1)
        main_b.append(pioche[x])
        del pioche[x]
    # Defausse
    defausse = []
    return(plateau, l_roi, c_roi, main_r, main_b, pioche, defausse)

##########################################
##### 2 - Affichage de l'état du jeu #####
##########################################
def afficher_main(couleur, main):
    main=" ".join(main)
    if couleur == "Rouge" :
        print ("Rouge : "+ main)
    else :
        print ("Blanc :" , main)

def afficher_jeu(plateau, l_roi, c_roi, main_r, main_b):
    # Affichage des mains des 2 joueurs :
    afficher_main("Rouge", main_r)
    afficher_main("Blanc", main_b)
    # Affichage du plateau :
    nb_colonnes = len(plateau[1])
    for k in range(len(plateau)):
        print((("-")*nb_colonnes*5) + ("-"), end = '\n')
        for l in range(len(plateau[k])):
            # Affichage si aucun pion n'est présent sur la case :
            if plateau[k][l] == '':
                print("|  ",plateau[k][l],"",end = '')
            # Affichage avec le roi  + un pion :
            elif len(plateau[k][l]) == 2:
                print("|",plateau[k][l],"", end = '')
            # Affichage avec le roi seul OU un pion seul :
            elif plateau[k][l] == 'R' or plateau[k][l] == 'B' or plateau[k][l] == 'X':
                if plateau[k][l] == 'X':
                    print("|",plateau[k][l]," ", end = '')
                else:
                    print("| ",plateau[k][l],"", end = '')
        print("|\n", end = '')
    print((("-")*nb_colonnes*5) + ("-"), end = '\n')

##########################################
##### 3 - Mouvement du roi possible ######
##########################################
def mouvement_possible(plateau, l_roi, c_roi, carte):
    # On initie deux variables nb_lignes, nb_colonnes permettant
    # de poser les limites du plateau
    nb_lignes = len(plateau)
    nb_colonnes = len(plateau[0])
    # Pour chaque carte possible du jeu et pour chaque position on
    # return False si le mouvement n'est pas possible
    # et True si il l'est.
    # 1/ Cartes "N"
    if carte == "N1":
        # Si la ligne du roi = 0 dans ce cas on arrive en dehors du plateau
        # --> pas possible
        if l_roi == 0:
            return False
        l_roi = l_roi - 1
        # Si un pion est présent sur les nouvelles coordonnées après le déplacement avec na carte
        # --> pas possible
        if plateau[l_roi][c_roi] != "":
            return False
        return True
        # Sinon, on joue, le déplacement est possibles
    elif carte == "N2":
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "N3":
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 2/ Cartes "NE"
    elif carte == "NE1":
        if c_roi == nb_colonnes - 1:
            return False
        elif l_roi == nb_colonnes - 1:
            return False
        l_roi = l_roi - 1
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NE2":
        if c_roi > nb_colonnes - 3:
            return False
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        C_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NE3":
        if c_roi > nb_colonnes - 4:
            return False
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 3/ Cartes "E"
    elif carte == "E1":
        if c_roi == nb_colonnes - 1:
            return False
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "E2":
        if c_roi > nb_colonnes - 3:
            return False
        c_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "E3":
        if c_roi > nb_colonnes - 4:
            return False
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 4/ Cartes "SE":
    elif carte == "SE1":
        if c_roi == nb_colonnes - 1:
            return False
        if l_roi == nb_lignes - 1:
            return False
        l_roi = l_roi + 1
        c_roi = c_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SE2":
        if c_roi > nb_colonnes - 3:
            return False
        if l_roi > nb_lignes - 3:
            return False
        l_roi = l_roi + 2
        c_roi = c_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SE3":
        if c_roi > nb_colonnes - 4:
            return False
        if l_roi > nb_lignes - 4:
            return False
        l_roi = l_roi + 3
        c_roi = c_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 5/ Cartes "S"
    elif carte == "S1":
        if l_roi == nb_lignes - 1:
            return False
        l_roi = l_roi + 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "S2":
        if l_roi > nb_lignes - 3:
            return False
        l_roi = l_roi + 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "S3":
        if l_roi > nb_lignes - 4:
            return False
        l_roi = l_roi + 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 6/ Cartes "SO"
    elif carte == "SO1":
        if l_roi == nb_lignes - 1:
            return False
        if c_roi == 0:
            return False
        l_roi = l_roi + 1
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SO2":
        if l_roi > nb_lignes - 3:
            return False
        if c_roi < 2:
            return False
        l_roi = l_roi + 2
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "SO3":
        if l_roi > nb_lignes - 4:
            return False
        if c_roi < 3:
            return False
        l_roi = l_roi + 3
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 7/ Cartes "O"
    elif carte == "O1":
        if c_roi == 0:
            return False
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "O2":
        if c_roi < 2:
            return False
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "O3":
        if c_roi < 3:
            return False
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    # 8/ Cartes "NO"
    elif carte == "NO1":
        if c_roi == 0:
            return False
        if l_roi == 0:
            return False
        l_roi = l_roi - 1
        c_roi = c_roi - 1
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NO2":
        if c_roi < 2:
            return False
        if l_roi < 2:
            return False
        l_roi = l_roi - 2
        c_roi = c_roi - 2
        if plateau[l_roi][c_roi] != "":
            return False
        return True
    elif carte == "NO3":
        if c_roi < 3:
            return False
        if l_roi < 3:
            return False
        l_roi = l_roi - 3
        c_roi = c_roi - 3
        if plateau[l_roi][c_roi] != "":
            return False
        return True

##########################################
########### 4 - Main jouable #############
##########################################
def main_jouable(plateau, l_roi, c_roi, main):
    liste_main_jouable = []
    for i in range(len(main)):
        x = mouvement_possible(plateau, l_roi, c_roi, main[i])
        if x == True:
            liste_main_jouable.append(main[i])
    return liste_main_jouable
# main_jouable renvoie la liste des cartes de la main pour lesquelles le 
# mouvement est possible, si aucun n'est possible alors une liste vide est return

###################################################
######## 5 - Demande une action à un joueur #######
###################################################
def demande_action(couleur, plateau, l_roi, c_roi, main):
    choix_carte = input ("Vous êtes le joueur "+couleur+", que voulez vous faire ? ")
    while True :
        # Si le choix est "pioche" on vérifie que le joueur n'a pas 5 cartes déjà dans sa main
        # Si c'est le cas --> impossible de piocher
        if choix_carte == "pioche":
            if len(main) == 5:
                choix_carte = input("Action impossible, que souhaitez-vous faire ? ")
            else:
                break
        elif choix_carte == "passe":
            # Si le choix est "passe" on vérifie qu'il ne peut pas piocher + qu'il ne peut pas jouer une seule
            # des cartes de sa main
            for _ in main:
                if len(main_jouable(plateau, l_roi, c_roi, main)) != 0 and len(main) < 5:
                    choix_carte = input("Action impossible, que souhaitez-vous faire ? ") 
            return choix_carte
        # Si le choix n'est pas "pioche" ou "passe", on vérifie que le choix est une carte jouable dans sa main
        else :
            if choix_carte not in main and choix_carte != main:
                choix_carte = input("Action impossible, que souhaitez-vous faire ? ") 
            else :
                if mouvement_possible(plateau, l_roi, c_roi, choix_carte) == False:
                    choix_carte = input("Action impossible, que souhaitez-vous faire ? ")
                else:
                    break
    return choix_carte

##########################################
############ 6 - Bouge le roi ############
##########################################
# Fonction préliminaire pour bouge_le_roi permettant de déplacer le roi
def mouvement_pionroi(couleur, plateau, l_roi, c_roi, carte):
    if mouvement_possible(plateau, l_roi, c_roi, carte) == True:
        # Enlever le roi de la position d'origine (l_roi, c_roi)
        roi = "X"
        plateau[l_roi][c_roi] = "".join(x for x in plateau[l_roi][c_roi] if x not in roi)
        # En fonction des cartes, ajouter le pion + le roi (si le mouvement est possible)
        if len(carte) == 2:
            direction = carte[0]
            case = carte[1]
            case = int(case)
            if direction == "N":
                l_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "S":
                l_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "E":
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "O":
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
        elif len(carte) == 3:
            direction = carte[0:2]
            case = carte[2]
            case = int(case)
            if direction == "NE":
                l_roi -= case
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "NO":
                l_roi -= case
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "SE":
                l_roi += case
                c_roi += case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
            elif direction == "SO":
                l_roi += case
                c_roi -= case
                if couleur == 'Rouge':
                    plateau[l_roi][c_roi] = 'XR'
                else:
                    plateau[l_roi][c_roi] = 'XB'
    return(plateau, l_roi, c_roi)

def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, couleur):
    # Pour déplacer le roi et mettre le pion de la bonne couleur
    x = mouvement_pionroi(couleur, plateau , l_roi, c_roi, carte)
    # On récupère les variables de mouvement_pionroi pour les mettre à jour pour la suite du jeu
    plateau = x[0]
    l_roi = x[1]
    c_roi = x[2]
    # Retirer la carte jouée de la main du joueur
    if couleur == "Rouge":
        index_carte = main_r.index(carte)
        del main_r[index_carte]
    if couleur == "Blanc":
        index_carte = main_b.index(carte)
        del main_b[index_carte]
    # Ajouter la carte jouée dans la défausse
    defausse.append(carte)
    return(plateau, l_roi, c_roi, main_r, main_b, defausse)

###############################################
######## 7 - Définition des territoires #######
###############################################
def territoire(plateau, ligne, colonne, couleur):
    territoire = []
    # Pour le joueur Blanc
    if couleur == "Blanc": 
        pos_Blanc_plateau = []
        # S'il n'y a pas de pion Blanc sur la case (ligne, colonne), renvoie une liste vide :
        if plateau[ligne][colonne] != 'B' and plateau[ligne][colonne] != 'XB':
            return territoire
        # Lister toutes les positions des pions blancs ('B' ou 'XB) sur le plateau :
        else:
            for i in range(len(plateau)):
                for j in range(len(plateau[i])):
                    if plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                        pos_Blanc_plateau.append((i,j))
                # Algo pour trouver le territoire blanc à partir de la case (ligne, colonne):
                territoire_Blanc = [(ligne, colonne)]
                q = 0
                while q < len(territoire_Blanc):
                    ligne = territoire_Blanc[q][0]
                    colonne = territoire_Blanc[q][1]
                    # Pour la première itération (ligne, colonne du départ) on regarde les côtés adjacents (pas de diagonales)
                    # si un pion de la couleur blanc est détecté alors on prend ce pion et on refait la même chose
                    l = [(ligne-1, colonne), (ligne+1, colonne), (ligne, colonne-1), (ligne, colonne+1)]
                    for elem_l in range(len(l)):
                        for elem_pion in range(len(pos_Blanc_plateau)):
                            if l[elem_l] == pos_Blanc_plateau[elem_pion]:
                                if l[elem_l] not in territoire_Blanc:
                                    territoire_Blanc.append(l[elem_l]) 
                    q += 1               
            return territoire_Blanc
    elif couleur == "Rouge": 
        pos_Rouge_plateau = []
        # S'il n'y a pas de pion Blanc sur la case (ligne, colonne), renvoie une liste vide:
        if plateau[ligne][colonne] != 'R' and plateau[ligne][colonne] != 'XR':
            return territoire
        # Lister toutes les positions des pions rouges ('R' ou 'XR') sur le plateau :
        else:
            for i in range(len(plateau)):
                for j in range(len(plateau[i])):
                    if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                        pos_Rouge_plateau.append((i,j))
                    # Algo pour trouver le territoire Rouge à partir de (ligne, colonne):
                    territoire_Rouge = [(ligne, colonne)]
                    q = 0
                    while q < len(territoire_Rouge):
                        ligne = territoire_Rouge[q][0]
                        colonne = territoire_Rouge[q][1]
                        l = [(ligne-1, colonne), (ligne+1, colonne), (ligne, colonne-1), (ligne, colonne+1)]
                        for elem_l in range(len(l)):
                            for elem_pion in range(len(pos_Rouge_plateau)):
                                if l[elem_l] == pos_Rouge_plateau[elem_pion]:
                                    if l[elem_l] not in territoire_Rouge:
                                        territoire_Rouge.append(l[elem_l]) 
                        q += 1                       
            return territoire_Rouge

##########################################
############## 8 - Scores ################
##########################################
def score(plateau, couleur):
    # Pour le joueur Rouge
    if couleur == "Rouge":
        # Lister toutes les positions du jeu avec des pions rouges
        pos_Rouge_plateau = []
        for i in range(len(plateau)):
            for j in range(len(plateau[i])):
                if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                    pos_Rouge_plateau.append((i,j))
        # Algo du score pour le joueur Rouge
        score_Rouge = 0
        m = len(pos_Rouge_plateau) - 1
        while m > -1:
            terr = territoire(plateau, pos_Rouge_plateau[m][0], pos_Rouge_plateau[m][1], "Rouge")
            terr2 = []
            for elem in terr:
                if elem in pos_Rouge_plateau:
                    terr2.append(elem)
            score_Rouge += len(terr2)**2
            for elem in terr2:
                pos_Rouge_plateau.remove(elem)  
            m -= len(terr2)
        return score_Rouge
    elif couleur == "Blanc":
        # Lister toutes les positions du jeu avec des pions blancs
        pos_Blanc_plateau = []
        for i in range(len(plateau)):
            for j in range(len(plateau)):
                if plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                    pos_Blanc_plateau.append((i,j))
        # Algo du score pour le joueur Blanc
        score_Blanc = 0
        m = len(pos_Blanc_plateau) - 1
        while m > -1:
            terr = territoire(plateau, pos_Blanc_plateau[m][0], pos_Blanc_plateau[m][1], "Blanc")
            terr2 = []
            for elem in terr:
                if elem in pos_Blanc_plateau:
                    terr2.append(elem)
            score_Blanc += len(terr2)**2
            for elem in terr2:
                pos_Blanc_plateau.remove(elem)
            m -= len(terr2)
        return score_Blanc

# Fonction additionnelle pour trouver les coordonnées du roi après avoir jouer une carte 
def coord_carte(plateau, l_roi, c_roi, carte):
    if mouvement_possible(plateau, l_roi, c_roi, carte) == True:
        if len(carte) == 2:
            direction = carte[0]
            case = carte[1]
            case = int(case)
            if direction == "N":
                l_roi -= case
            elif direction == "S":
                l_roi += case
            elif direction == "E":
                c_roi += case
            elif direction == "O":
                c_roi -= case
        elif len(carte) == 3:
            direction = carte[0:2]
            case = carte[2]
            case = int(case)
            if direction == "NE":
                l_roi -= case
                c_roi += case
            elif direction == "NO":
                l_roi -= case
                c_roi -= case
            elif direction == "SE":
                l_roi += case
                c_roi += case
            elif direction == "SO":
                l_roi += case
                c_roi -= case
    return(l_roi, c_roi)

##########################################
############## Partie IA #################
##########################################
def name():
    return "Python Royal"

# Le but de cette IA est de trouver le coup qui rapporte le plus gros score pour chaque tour et celui qui est 
# le plus optimal si plusieurs coups emmènent à un score maximal 
def play(plateau , l_roi , c_roi , main_r , main_b , couleur):
    main = main_b
    if couleur == "Rouge":
        main = main_r
    # On définie notre main jouable
    main_jouable_joueur = main_jouable(plateau, l_roi, c_roi, main)
    if len(main_jouable_joueur) == 0:
        if len(main) < 5:
            return "pioche"
        else:
            return "passe"
    # Algo principal de l'IA : 
    score_carte_main = {}
    lignecolonne_carte_main = {}
    for carte in main_jouable_joueur:
    # Etablissement des scores potentiels pour tous les coups de la main jouable
        score_avant_mouvement = score(plateau, couleur)
        mouvement = coord_carte(plateau, l_roi, c_roi, carte)
        new_ligne_roi = mouvement[0]
        new_colonne_roi = mouvement[1]
        if couleur == "Rouge":
            plateau[new_ligne_roi][new_colonne_roi] = "XR"
        elif couleur == "Blanc":
            plateau[new_ligne_roi][new_colonne_roi] = "XB"
        lignecolonne_carte_main[carte] = (new_ligne_roi, new_colonne_roi)
        score_apres_mouvement = score(plateau, couleur)
        score_carte_main[carte] = score_apres_mouvement
        plateau[new_ligne_roi][new_colonne_roi] = ""
    # On regarde le coup qui nous rapporte le plus de score parmi tous les coups possibles :
    max_score_carte_main = max(score_carte_main, key = lambda key : score_carte_main[key])
    score_maximal = score_carte_main[max_score_carte_main]
    difference_score_joueur = score_carte_main[max_score_carte_main] - score_avant_mouvement
    # On regarde combien de carte emmène à un score maximal
    nb_carte_avec_score_maximal = []
    for i in score_carte_main:
        if score_carte_main[i] == score_maximal:
            nb_carte_avec_score_maximal.append(i)
    # On calcul le pourcentage correspondant à la différence de score obtenu pour une carte donnée
    if score_avant_mouvement == 0:
        pourcentage = int(difference_score_joueur*100)
    else:
        pourcentage = int((difference_score_joueur/score_avant_mouvement)*100)
    # Si on a une seule carte qui ammène à un score maximal alors :
    if len(nb_carte_avec_score_maximal) == 1:
        if difference_score_joueur == 1:
            if len(main) < 3:
                return "pioche"
            if pourcentage <= 3 and len(main) < 5:
                return "pioche"
            elif pourcentage <= 3 and len(main) == 5:
                return max_score_carte_main
            elif pourcentage > 3:
                return max_score_carte_main
        elif difference_score_joueur <= 10:
            if pourcentage <= 5 and len(main) < 5:
                return "pioche"
            elif pourcentage <= 5 and len(main) == 5:
                return max_score_carte_main
            elif pourcentage > 5:
                return max_score_carte_main
        elif difference_score_joueur > 10:
            return max_score_carte_main
    # Si on a plusieurs cartes qui emmènent à un score maximal, alors il faut en choisir la meilleure
    # par rapport à la présence ou l'absence de nos pions sur la nouvelle position et la quantité de nos
    # pions présents 
    elif len(nb_carte_avec_score_maximal) > 1:
        # Initialisation de toutes les variables
        recap_ensemble_carte = dict()
        pions_proches_reels_joueur = []
        pions_proches_reels_joueur_diagonales = []
        ensemble_coord_pion_joueur = []
        for carte in nb_carte_avec_score_maximal:
    # Etablissement des scores potentiels pour tous les coups de la main jouable
            score_avant_mouvement = score(plateau, couleur)
            mouvement = coord_carte(plateau, l_roi, c_roi, carte)
            new_ligne_roi = mouvement[0]
            new_colonne_roi = mouvement[1]
            lignecolonne_carte_main[carte] = (new_ligne_roi, new_colonne_roi)
            if couleur == "Rouge":
                plateau[new_ligne_roi][new_colonne_roi] = "XR"
                for i in range(len(plateau)):
                    for j in range(len(plateau[i])):
                        if plateau[i][j] == "R" or plateau[i][j] == "XR":
                            ensemble_coord_pion_joueur.append((i,j))
            elif couleur == "Blanc":
                plateau[new_ligne_roi][new_colonne_roi] = "XB"
                for i in range(len(plateau)):
                    for j in range(len(plateau[i])):
                        if plateau[i][j] == "B" or plateau[i][j] == "XB":
                            ensemble_coord_pion_joueur.append((i,j))
            # On détermine les pions théoriques dans toutes les directions d'une case
            pions_proches_theoriques = [(new_ligne_roi - 1, new_colonne_roi), (new_ligne_roi, new_colonne_roi - 1), (new_ligne_roi + 1, new_colonne_roi), (new_ligne_roi, new_colonne_roi +1)]
            pions_proches_theoriques_diagonales = [(new_ligne_roi -1, new_colonne_roi -1), (new_ligne_roi - 1, new_colonne_roi +1), (new_ligne_roi +1, new_colonne_roi -1), (new_ligne_roi +1, new_colonne_roi +1)]
            # On regarde si avec notre coup on trouve des pions à nous qui sont proches (1 case dans toutes les 
            # directions)
            for elem in range(len(ensemble_coord_pion_joueur)):
                for m in range(len(pions_proches_theoriques)):
                    if pions_proches_theoriques[m] == ensemble_coord_pion_joueur[elem]:
                        pions_proches_reels_joueur.append(pions_proches_theoriques[m])
                pions_proches_reels_joueur = list(set(pions_proches_reels_joueur))
                for n in range(len(pions_proches_theoriques_diagonales)):
                    if pions_proches_theoriques_diagonales[n] == ensemble_coord_pion_joueur[elem]:
                        pions_proches_reels_joueur_diagonales.append(pions_proches_theoriques_diagonales[n])
                pions_proches_reels_joueur_diagonales = list(set(pions_proches_reels_joueur_diagonales))
            # Avec les informations des pions présents ou non de l'adversaire et surtout des nôtres :
            # on choisi le meilleur coup avec un score maximal selon le nombre de nos pions présents pour 
            # construire un potentiel territoire (si un de nos pions se trouve sur la diagonale)
            score_apres_mouvement = score(plateau, couleur)
            score_carte_main[carte] = score_apres_mouvement
            plateau[new_ligne_roi][new_colonne_roi] = ""
            # recap_ensemble_carte récapitule pour toutes les cartes qui emmène à un score maximal :
            # - le nombre de pions proches de notre couleur (N/S/E/O): pions_proches_reels_joueur
            # - le nombre de pions proches en diagonale de notre couleur (NO/NE/SE/SO) : pions_proches_reels_joueur_diagonales
            recap_ensemble_carte[carte] = (len(pions_proches_reels_joueur), len(pions_proches_reels_joueur_diagonales))
            pions_proches_reels_joueur = []
            pions_proches_reels_joueur_diagonales = []
        # On fait un algo pour choisir entre les cartes :
        nb_pions_joueur_case_adjacente = []
        nb_pions_joueur_case_adjacente_diagonales = []
        nb_total_pions_adjacents_joueur = []
        # Pour toutes les cartes avec un score maximal
        for carte in nb_carte_avec_score_maximal:
            nb_pions_joueur_case_adjacente.append(recap_ensemble_carte[carte][0])
            nb_pions_joueur_case_adjacente_diagonales.append(recap_ensemble_carte[carte][1])
            nb_total_pions_adjacents_joueur.append(recap_ensemble_carte[carte][0] + recap_ensemble_carte[carte][1])
        max_nb_total_pions_adjacents_joueur = max(nb_total_pions_adjacents_joueur)
        index = nb_total_pions_adjacents_joueur.index(max_nb_total_pions_adjacents_joueur)
        # Différent choix selon les résultats de l'algo
        if difference_score_joueur == 1:
            if nb_pions_joueur_case_adjacente_diagonales == 0:
                return nb_carte_avec_score_maximal[index]
            elif len(main) < 3 and max_nb_total_pions_adjacents_joueur < 2:
                return "pioche"
            elif len(main) < 3 and max_nb_total_pions_adjacents_joueur >= 2:
                return nb_carte_avec_score_maximal[index]
            elif pourcentage <= 3 and len(main) < 5:
                return "pioche"
            elif pourcentage <= 3 and len(main) == 5:
                return nb_carte_avec_score_maximal[index]
            elif pourcentage > 3:
                return nb_carte_avec_score_maximal[index]
        elif difference_score_joueur < 10:
            if pourcentage <= 5 and len(main) < 5 and max_nb_total_pions_adjacents_joueur < 2:
                return "pioche"
            elif pourcentage <= 5 and len(main) == 5:
                return nb_carte_avec_score_maximal[index]
            elif pourcentage > 5 and max_nb_total_pions_adjacents_joueur > 1:
                return nb_carte_avec_score_maximal[index]
        elif difference_score_joueur > 10:
            return nb_carte_avec_score_maximal[index]
        return nb_carte_avec_score_maximal[index]


def main ():
    # Initialisation de toutes les variables 
    init = init_jeu(9,9)
    plateau = init[0]
    l_roi = init[1]
    c_roi = init[2]
    main_r = init[3]
    main_b = init[4]
    pioche = init[5]
    defausse = init[6]
    # Initialisation des variables "passe"
    passe = []
    # Début de la boucle de jeu principale
    x = True
    # Le rouge commence toujours
    couleur = "Rouge"
    main = main_r
    nombre_tour = 0
    while x == True :
        compteur_pion_rouge = 0
        compteur_pion_blanc = 0
        for i in range(len(plateau)):
            for j in range(len(plateau[i])):
                if plateau[i][j] == 'R' or plateau[i][j] == 'XR':
                    compteur_pion_rouge += 1
                elif plateau[i][j] == 'B' or plateau[i][j] == 'XB':
                    compteur_pion_blanc += 1
        if compteur_pion_blanc == 26 and compteur_pion_rouge == 26:
            x = False
        elif len(passe) == 2:
            x = False
        elif nombre_tour > 0:
            if couleur == "Rouge":
                couleur = "Blanc"
                main = main_b
            elif couleur == "Blanc":
                couleur = "Rouge"
                main = main_r
        elif compteur_pion_rouge == 26 and compteur_pion_blanc < 26:
            couleur = "Blanc"
            main = main_b
        elif compteur_pion_blanc == 26 and compteur_pion_rouge < 26:
            couleur = "Rouge"
            main = main_r
        afficher_jeu(plateau, l_roi, c_roi, main_r, main_b)
        action = play(plateau, l_roi, c_roi, main_r, main_b, couleur)
        if action == "passe":
            passe.append(couleur)
            passe = list(set(passe))
            nombre_tour += 1
        elif action == "pioche":
            # Si la pioche est vide, alors on transfère le contenu de la défausse dans 
            # la pioche et on mélange
            if len(pioche) == 0:
                i = len(defausse) - 1 
                while i > -1:
                    pioche.append(defausse[i])
                    del defausse[i]
                    i -= 1
                random.shuffle(pioche)
            # Si la pioche n'est pas vide:
            pioche_carte = random.choice(pioche)
            main.append(pioche_carte)
            i = pioche.index(pioche_carte)
            del pioche[i]
            nombre_tour += 1
        else :
            mouvement = bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, action, couleur)
            plateau = mouvement[0]
            l_roi = mouvement[1]
            c_roi = mouvement[2]
            nombre_tour += 1
    score_Rouge = score(plateau, "Rouge")
    score_Blanc = score(plateau, "Blanc")
    print("Rouge", score_Rouge)
    print("Blanc", score_Blanc)
    if score_Rouge > score_Blanc:
        print("Rouge a gagné la partie")
    elif score_Rouge == score_Blanc:
        print("Égalité") 
    else:
        print("Blanc a gagné la partie")

main()
